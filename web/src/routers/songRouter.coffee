###
# Le Müsik Backbone.js song router Written in Coffeescript
# v 1.0.0
# Mikko Majuri (majuri.mikko@gmail.com)
###
sc = require '../collections/songCollection'
sm = require '../models/songModel'
sv = require '../views/songView'

AppRouter = Backbone.Router.extend ->

	routes: 
		''					: 'list'
		'songs/:id'	: 'songDetails'

	list : ->
		@songList			= new sc.songCollection()
		@songListView	=  new sv.songListView()
		@songList.fetch()
		$('#mainSongList').html @songListView.render().el

	songDetails : (id) ->
		@song			=	@songList.get id
		@songView	= new songView {model : @song}
		$('#content').html @songView.render().el
