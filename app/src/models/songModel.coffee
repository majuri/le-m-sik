###
# Le Müsik song model class. Written in Coffeescript
# v 1.0.0
# Mikko Majuri (majuri.mikko@gmail.com)
###

models =  require './models'

class Song
	constructor	: (@name, @url) ->
		console.log Date.now() + ': ADDED NEW SONG '+ @name + ' from ' + @url

	saveToDB : (callback) ->
		song = new models.Song
			name	:	@name
			url		:	@url

		song.save (err) ->
			if err
				console.log err
				throw err
			else
				console.log console.log 'SONG: Song saved successfully'
				callback null, song

exports.Song = Song
